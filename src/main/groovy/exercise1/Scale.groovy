package exercise1

class Scale {
    static final MAX_LOAD = 1000.0

    private BigDecimal currentLoad = 0.0

    Scale(BigDecimal initialLoad = 0.0) {
        if(initialLoad > MAX_LOAD) {
            throw new IllegalArgumentException("exercise1.Scale overloaded: cannot start with weight over $MAX_LOAD kg.")
        }
        if(initialLoad < 0) {
            throw new IllegalArgumentException("exercise1.Scale negative: cannot start scale with negative load.")
        }
        currentLoad = initialLoad
    }

    BigDecimal addLoad(BigDecimal load) {
        if (currentLoad + load > MAX_LOAD) {
            throw new IllegalArgumentException("exercise1.Scale overload: Load exceeding $MAX_LOAD kg. Current load is $currentLoad")
        }
        if (load >= 0) {
            currentLoad += load
        } else {
            removeLoad(-load)
        }
    }

    BigDecimal removeLoad(BigDecimal load) {
        if (currentLoad - load < 0) {
            throw new IllegalArgumentException("exercise1.Scale negative: Load cannot be negative. Current load is $currentLoad")
        }
        if (load >= 0) {
            currentLoad -= load
        } else {
            addLoad(-load)
        }
    }

    BigDecimal getCurrentLoad() {
        currentLoad
    }
}

