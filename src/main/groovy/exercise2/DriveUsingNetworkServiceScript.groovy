package exercise2
// This script will mimic a live system

def net = new NetworkProviderService() // Try with online: false
def data = new DataProviderService(networkProviderService: net)

def service = new UsingNetworkService(provider: data)

println service.getCountiesWithNameLike("jylland")
println service.getMunicipalsWithRegionAndNameLike("Region Midtjylland", "Aar")
