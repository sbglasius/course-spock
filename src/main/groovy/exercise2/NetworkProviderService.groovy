package exercise2

class NetworkProviderService {
    boolean online = true

    String getUrlText(URL url) {
        if(!online) {
            throw new IOException("Not connected")
        }
        url.text
    }
}
