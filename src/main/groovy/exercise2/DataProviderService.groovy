package exercise2

import groovy.json.JsonSlurper
import groovy.transform.Memoized

class DataProviderService {
    NetworkProviderService networkProviderService

    @Memoized
    List<Map> getCounties() {
        println "Fetching counties"
        def responseText = networkProviderService.getUrlText("https://dawa.aws.dk/regioner".toURL())
        new JsonSlurper().parseText(responseText)
    }

    @Memoized
    List<String> getMunicipalsInCounty(Integer countyNumber) {
        println "Fetching municipals for county number $countyNumber"
        def responseText = networkProviderService.getUrlText("https://dawa.aws.dk/kommuner?regionskode=${countyNumber}".toURL())
        new JsonSlurper().parseText(responseText)
    }


    Map getCountyByName(String countyName) {
        counties.find { it.navn == countyName }
    }

    List<String> getMunicipalsInCounty(String countyName) {
        def county = getCountyByName(countyName)
        if (county) {
            getMunicipalsInCounty(county.kode as Integer)
        }
    }

}
