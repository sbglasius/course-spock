package exercise2

class UsingNetworkService {
    DataProviderService provider

    def getCountiesWithNameLike(String name) {
        provider.counties.findAll {
            it.navn ==~ /.*($name).*/
        }
    }

    def getMunicipalsWithRegionAndNameLike(String regionName, String name) {
        def municipals = provider.getMunicipalsInCounty(regionName)
        municipals?.findAll {
            it.navn ==~ /.*($name).*/
        }
    }
}
