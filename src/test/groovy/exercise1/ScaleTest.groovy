package exercise1

import exercise1.Scale
import spock.lang.Specification
import spock.lang.Unroll

class ScaleTest extends Specification {
    def "test adding load to an empty scale will return the load added"() {
        setup:
        Scale scale = new Scale()

        when:
        def result = scale.addLoad(50)

        then:
        result == 50
        scale.currentLoad == 50
    }

    def "test adding load to a loaded scale will return the load + the added load"() {
        setup:
        Scale scale = new Scale(20)
        when:
        def result = scale.addLoad(50)

        then:
        result == 70
        scale.currentLoad == 70
    }

    @Unroll("exercise1.Scale with #initialLoad kg load added #addedLoad kg yields #intermediateLoad kg then removing #removedLoad kg yields final #finalLoad kg")
    def "test calculations on the scale returns the expected load"() {
        setup:
        Scale scale = new Scale(initialLoad)

        expect:
        scale.currentLoad == initialLoad

        when:
        def result = scale.addLoad(addedLoad)

        then:
        result == intermediateLoad

        when:
        result = scale.removeLoad(removedLoad)

        then:
        result == finalLoad
        where:
        initialLoad | addedLoad | intermediateLoad | removedLoad | finalLoad
        0           | 500       | 500              | 500         | 0
        500         | 200       | 700              | 300         | 400
        500         | -200      | 300              | -300        | 600
    }

    @Unroll("alternative exercise1.Scale with #initialLoad kg load added #addedLoad kg yields #intermediateLoad kg then removing #removedLoad kg yields final #finalLoad kg")
    def "alternative test calculations on the scale returns the expected load"() {
        setup:
        Scale scale = new Scale(initialLoad)

        expect:
        scale.currentLoad == initialLoad

        and:
        scale.addLoad(addedLoad) == intermediateLoad

        and:
        scale.removeLoad(removedLoad) == finalLoad
        where:
        initialLoad | addedLoad | intermediateLoad | removedLoad | finalLoad
        0           | 500       | 500              | 500         | 0
        500         | 200       | 700              | 300         | 400
        500         | -200      | 300              | -300        | 600
    }

    @Unroll('Starting with #initialLoad and #operation #weight yields exception with message #expectedMessage')
    def "test cases where an exception will be thrown"() {
        when:
        Scale scale = new Scale(initialLoad)

        and:
        scale.addLoad(weight)

        then:
        def e = thrown(IllegalArgumentException)
        e.message == expectedMessage

        where:
        initialLoad | weight | expectedMessage
        1200        | 0      | "exercise1.Scale overloaded: cannot start with weight over 1000.0 kg."
        -10         | 0      | "exercise1.Scale negative: cannot start scale with negative load."
        500         | 600    | "exercise1.Scale overload: Load exceeding 1000.0 kg. Current load is 500"
        500         | -600   | "exercise1.Scale negative: Load cannot be negative. Current load is 500"

        // Will return a text depending on the added weight
        operation = weight >= 0 ? 'addLoad' : 'removeLoad'
    }
}
