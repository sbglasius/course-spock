package exercise2

import spock.lang.Specification

class DataProviderServiceWithoutMockingTest extends Specification {
    def sut = new DataProviderService(networkProviderService: new NetworkProviderService())

    def "test get counties retrieves the right amount of counties for Denmark"() {
        expect: "Fix this test"
        sut.counties.size() == 6
    }

    def "test that there are the right number of municipals in Region Midtjylland by number"() {
        expect: "Fix this test"
        sut.getMunicipalsInCounty("Region Sjælland").size() == 19

    }

    def "test that there are the right number of municipals in Region Midtjylland"() {
        expect: "Fix this test."
        sut.getMunicipalsInCounty(1080).size() == 19
    }

    def "test that fetching we can get the county by name"() {
        expect: "Fix this test."
        sut.getCountyByName("Region Midtjylland").nr == "1082"
    }
}
