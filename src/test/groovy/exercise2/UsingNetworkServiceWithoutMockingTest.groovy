package exercise2

import spock.lang.Specification

class UsingNetworkServiceWithoutMockingTest extends Specification {
    def sut = new UsingNetworkService()
    def setup() {
        sut.provider = new DataProviderService(networkProviderService: new NetworkProviderService())
    }

    def "test getting data for counties with 'jylland' in the name"() {
        expect:
        sut.getCountiesWithNameLike('jylland').size() == 2
    }

    def "test getting data for municipals in Midtjylland and 'AAr' in the name"() {
        when:
        def data = sut.getMunicipalsWithRegionAndNameLike("Region Midtjylland","Aar")

        then:
        data.size() == 1
        data[0].navn == "Aarhus"
    }
}
